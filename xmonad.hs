import XMonad 
import Control.Concurrent (threadDelay)
import Data.Time.Clock
import XMonad.Prompt
import Graphics.X11
import Graphics.X11.ExtraTypes.XF86 
import XMonad.Util.EZConfig
import XMonad.Config.Xfce
import XMonad.Util.Run (spawnPipe)
import System.IO (hPutStrLn)
import GHC.IOBase (Handle)
import Control.Monad (liftM2)

import XMonad.Actions.Promote
import XMonad.Actions.UpdatePointer
import XMonad.Actions.Warp
import XMonad.Actions.SpawnOn
import XMonad.Actions.CopyWindow
import XMonad.Actions.GridSelect
import XMonad.Actions.DynamicWorkspaces
import XMonad.Actions.CopyWindow(copy)

import qualified Data.Map as M
import qualified XMonad.StackSet as W

import XMonad.Hooks.UrgencyHook
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ICCCMFocus
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageHelpers

import XMonad.Layout.Circle 
import XMonad.Layout.Spacing 
import XMonad.Layout.Tabbed
import XMonad.Layout.Spiral
import XMonad.Layout.NoBorders
import XMonad.Layout.Maximize
import XMonad.Layout.Minimize
import XMonad.Layout.Spacing 
import XMonad.Layout.PerWorkspace
import XMonad.Layout.IM
import XMonad.Layout.Grid
import XMonad.Layout.ToggleLayouts

import Control.Monad (when)
import Data.Monoid
import Data.List
import Data.Maybe
import Data.Ratio ((%)) 

import Solarized

myDmenuSolarized = " -nb '" ++ solarizedBase03 ++ "'"
    ++ " -nf '" ++ solarizedBase00 ++ "'"
    ++ " -nb '" ++ solarizedBase03 ++ "'"
    ++ " -nf '" ++ solarizedYellow ++ "'"


main = do
	xmproc <- spawnPipe "/usr/bin/xmobar ~/.xmonad/xmobarrc"
	xmonad $ defaultConfig
		{ 
		  terminal = myTerminal
		, modMask = mod4Mask
		, keys = myKeys <+> keys xfceConfig
		, borderWidth        = 2
		, workspaces         = myWorkspaces
		, normalBorderColor  = solarizedBase01
		, focusedBorderColor = solarizedYellow
		, manageHook         = manageDocks <+> myManageHook 
		, layoutHook         = avoidStruts myLayouts
		, logHook = dynamicLogWithPP xmobarPP
		{
			  ppOutput = hPutStrLn xmproc
			, ppTitle = xmobarColor "green" "" . shorten 300
		}
		, handleEventHook = handleEventHook defaultConfig <+> fullscreenEventHook
        , startupHook = myStartProgram
		}
        
myTerminal = "lxterm"

myWorkspaces = [ "1:im" ,"2:www" ,"3:main" ,"4:work" ,"5:media",
                "6:syn" ,"7:doc" ,"8:music" ,"9:cal" ]

myProgList = [ "gvim" ,"gitg" ,"pidgin" ,"skype" ,"evolution",
      "synaptic-gksudo" ,"gparted-gksudo" ]

myKeys conf@(XConfig {XMonad.modMask = modMask}) = M.fromList $
    [ ((modMask, xK_q ), kill)
	, ((modMask .|. shiftMask, xK_i ), spawn "firefox")
	, ((modMask .|. shiftMask, xK_h ), spawn "thunar")
	, ((modMask .|. shiftMask, xK_t ), spawn "lxtask")
	, ((0, xK_Print), spawn "xfce4-screenshooter -f -s /home/thinker/Pictures/screen/")
    , ((modMask, xK_Print), spawn "xfce4-screenshooter -w -s /home/thinker/Pictures/screen/")
	, ((modMask, xK_f ), spawnSelected defaultGSConfig myProgList)
	, ((modMask, xK_g ), goToSelected defaultGSConfig)
    , ((modMask, xK_space ), sendMessage NextLayout)
    , ((modMask .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)
    , ((modMask, xK_n ), refresh)
    , ((modMask, xK_Tab ), windows W.focusDown)
    , ((modMask, xK_j ), windows W.focusDown)
    , ((modMask, xK_k ), windows W.focusUp)
    , ((modMask, xK_m ), windows W.focusMaster)
    , ((modMask, xK_Return), windows W.swapMaster)
    , ((modMask .|. shiftMask, xK_j ), windows W.swapDown)
    , ((modMask .|. shiftMask, xK_k ), windows W.swapUp)
    , ((modMask, xK_h ), sendMessage Shrink)
    , ((modMask, xK_l ), sendMessage Expand)
    , ((modMask, xK_comma ), sendMessage (IncMasterN 1))
    , ((modMask, xK_period), sendMessage (IncMasterN (-1)))
    , ((modMask .|. shiftMask, xK_r ),
       broadcastMessage ReleaseResources >> restart "xmonad" True)
    , ((modMask .|. shiftMask, xK_l), spawn "xscreensaver-command -lock")
    , ((modMask, xK_p), spawn $ "dmenu_run " ++ myDmenuSolarized)
    --Mouse replace
    , ((modMask .|. shiftMask, xK_p), 
    gets (W.screen . W.current . windowset) >>= \x -> warpToScreen x  0 0)
	--Audio Info
	, ((modMask .|. shiftMask, xK_m), spawn "/home/thinker/scripts/notify.sh")
	--Audio Raise
	, ((0, 0x1008ff13), spawn "amixer -D pulse set Master 2%+")
    --Audio Lower
	, ((0, 0x1008ff11), spawn "amixer -D pulse set Master 2%-") 
    --Audio Mute
	, ((0, 0x1008ff12), spawn "amixer -D pulse set Master toggle")
    --Brightness UP
	, ((modMask .|. shiftMask, xK_a), spawn "xbacklight + 2")
	, ((modMask, xK_a), spawn "xbacklight + 20")
    --Brightness DOWN 
	, ((modMask .|. shiftMask, xK_d), spawn "xbacklight - 2")
	, ((modMask, xK_d), spawn "xbacklight - 20")
    , ((modMask .|. shiftMask, xK_BackSpace), removeWorkspace)
    , ((modMask .|. shiftMask, xK_v), selectWorkspace mXPConfig)
    , ((modMask, xK_m), withWorkspace mXPConfig (windows . W.shift))
    , ((modMask .|. shiftMask, xK_m), withWorkspace mXPConfig (windows . copy))
    {-, ((modMask .|. shiftMask, xK_r), renameWorkspace mXPConfig)-}
    ]
    ++
    zip (zip (repeat (modMask)) [xK_1..xK_9]) (map (withNthWorkspace W.greedyView) [0..])
    ++
    zip (zip (repeat (modMask .|. shiftMask)) [xK_1..xK_9]) (map (withNthWorkspace W.shift) [0..])

mXPConfig :: XPConfig
mXPConfig = defaultXPConfig { fgColor = solarizedBase2, bgColor = solarizedBase03, borderColor = solarizedBase01 }

myLayouts = onWorkspace "1:im" imLayout $ myTall ||| Mirror myTall
    ||| spiral goldProp ||| Grid ||| Full 
    where
        nmaster = 1
        ratio   = 1/2
        delta   = 3/100
        myTall = Tall nmaster delta ratio
        gridLayout = spacing 1 $ Grid 
        imLayout = withIM (14/100) (Role "buddy_list") gridLayout
        goldProp = (toRational (2/(1+sqrt(5)::Double))) 

------------------------------------------------------------------------
-- Window rules:
-- > xprop | grep WM_CLASS
myManageHook = composeAll 
    [ 
      className   =? "Pidgin"         --> doF(W.shift "1:im")
    , className   =? "Evolution"      --> doF(W.shift "9:cal")
    , title       =? "File Operation Progress" --> doFloat
	, className   =? "Skype-bin"      --> doF(W.shift "1:im")
	, className   =? "Synaptic"       --> doF(W.shift "6:syn")
	, className   =? "Xfce4-notifyd"  --> doIgnore
    , className   =? "Xfce4-panel"    --> doFloat
    , isFullscreen                    --> doFullFloat
    ] 

spawnToWorkspace :: String -> String -> X ()
spawnToWorkspace program workspace = do
                                      spawn program     
                                      windows $ W.greedyView workspace
myStartProgram = do
    spawnToWorkspace myTerminal "3:main" 
    spawnToWorkspace myTerminal "3:main" 
